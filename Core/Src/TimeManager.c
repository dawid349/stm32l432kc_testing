
#include "TimeManager.h"


void TimeControl(DelayTicks* Ticks);
static void IncreaseTicks(DelayTicks* Ticks);


volatile uint8_t INPUT = 0;
volatile uint8_t BT=0;
volatile uint8_t CAN_RQST = 0;
volatile uint8_t CAN_FRMAE = 0;

static void IncreaseTicks(DelayTicks* Ticks)
{
	/* Increase each tick */
	Ticks->InputsTick++;
	Ticks->CanSendFrameTick++;
	Ticks->CanRqstTick++;
	Ticks->BtSendTick++;

}

void TimeControl(DelayTicks* Ticks)
{
	/* Functions increasing and control current ticks for each periodical action */

	if(Ticks->InputsTick >= INPUT_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->InputsTick = 0; // reset tick for an action
		INPUT++;
	}

	if(Ticks->CanRqstTick >= CAN_RQST_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->CanRqstTick = 0; // reset tick for an action
		CAN_RQST++;
	}

	if(Ticks->CanSendFrameTick >= CAN_SEND_FRAME_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->CanSendFrameTick = 0; // reset tick for an action
		CAN_FRMAE++;
	}

	if(Ticks->BtSendTick >= BT_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->BtSendTick = 0; // reset tick for an action
		BT++;
	}


	/* Increase each tick */
	IncreaseTicks(Ticks);

}


