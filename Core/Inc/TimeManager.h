/*
 * TimeManager.h
 *
 *  Created on: 13 lis 2020
 *      Author: Dawid
 */

#ifndef INC_TIMEMANAGER_H_
#define INC_TIMEMANAGER_H_

#include <stdint.h>

/* definitions of os delays for each action in tick unit */

#define			INPUT_DELAY				2
#define			CAN_RQST_DELAY			4
#define			CAN_SEND_FRAME_DELAY	1
#define			BT_DELAY				10


typedef struct
{
	volatile int InputsTick;
	volatile int CanRqstTick;
	volatile int CanSendFrameTick;
	volatile int BtSendTick;

}DelayTicks;

void TimeControl(DelayTicks* Ticks);

#endif /* INC_TIMEMANAGER_H_ */
